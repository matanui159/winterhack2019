#include "minecraft.h"
#include "avcodec.h"
#include "internal.h"

static const uint8_t mcenc_colors[] = {
    233, 236, 236,
    240, 118,  19,
    189,  68, 179,
     58, 175, 217,
    248, 198,  39,
    112, 185,  25,
    237, 141, 172,
     62,  68,  71,
    142, 142, 134,
     21, 137, 145,
    121,  42, 172,
     53,  57, 157,
    114,  71,  40,
     84, 109,  27,
    161,  39,  34,
     20,  21,  25
};

typedef struct mcenc_context_t {
    int layer;
} mcenc_context_t;

static av_cold int mcenc_init(AVCodecContext* avctx) {
    mcenc_context_t* ctx = avctx->priv_data;
    ctx->layer = 0;
    return 0;
}

static mc_block_t* mcenc_block(mc_layer_t* layer, int x, int y) {
    return layer->blocks + y * layer->width + x;
}

static int mcenc_encode2(
    AVCodecContext* avctx, AVPacket* pkt,
    const AVFrame* frame, int* gotpkt
) {
    int ret;
    mcenc_context_t* ctx = avctx->priv_data;
    mc_layer_t* layer;
    int width = frame->width + 2;
    if (width * frame->height > MC_MAX_BLOCKS) {
        av_log(avctx, AV_LOG_ERROR, "Too many blocks\n");
        return AVERROR(EINVAL);
    }
    if ((ret = ff_alloc_packet2(avctx, pkt, sizeof(mc_layer_t), 0)) < 0) {
        return ret;
    }
    layer = (mc_layer_t*)pkt->data;
    memset(layer, 0, sizeof(mc_layer_t));

    layer->width = width;
    layer->height = frame->height;
    for (int y = 0; y < frame->height; ++y) {
        for (int x = 0; x < frame->width; ++x) {
            const uint8_t* pixel = frame->data[0] + y * frame->linesize[0] + x * 3;
            int min_wool = -1;
            int min_error;
            for (int w = 0; w < 16; ++w) {
                const uint8_t* wool = mcenc_colors + w * 3;
                int error =
                    abs(pixel[0] - wool[0]) +
                    abs(pixel[1] - wool[1]) +
                    abs(pixel[2] - wool[2]);
                if (min_wool == -1 || error < min_error) {
                    min_wool = w;
                    min_error = error;
                }
            }
            mcenc_block(layer, x, frame->height - 1 - y)->type = MC_BLOCK_WOOL + min_wool;
        }
    }

    if (ctx->layer % 2 == 0) {
        mc_block_t* cmd = mcenc_block(layer, frame->width, 1);
        cmd->type = MC_BLOCK_COMMAND;
        sprintf(
            cmd->command,
            "/clone ~-%i ~-1 ~ ~-1 ~%i ~ ~-%i ~-1 ~-%i",
            frame->width + 1,
            frame->height - 1,
            frame->width + 1,
            ctx->layer + 1
        );
        mcenc_block(layer, frame->width + 1, 0)->type = MC_BLOCK_WOOL;
        mcenc_block(layer, frame->width + 1, 1)->type = MC_BLOCK_REP2;
    } else {
        mc_block_t* cmd = mcenc_block(layer, frame->width + 1, 1);
        cmd->type = MC_BLOCK_COMMAND;
        sprintf(
            cmd->command,
            "/clone ~-%i ~-1 ~ ~-2 ~%i ~ ~-%i ~-1 ~-%i",
            frame->width + 2,
            frame->height - 1,
            frame->width + 2,
            ctx->layer + 1
        );
        mcenc_block(layer, frame->width, 0)->type = MC_BLOCK_WOOL;
        mcenc_block(layer, frame->width, 1)->type = MC_BLOCK_REP2;
    }
    ++ctx->layer;

    *gotpkt = 1;
    return 0;
}

AVCodec ff_minecraft_encoder = {
    .name = "minecraft",
    .long_name = NULL_IF_CONFIG_SMALL("Minecraft"),
    .type = AVMEDIA_TYPE_VIDEO,
    .id = AV_CODEC_ID_MINECRAFT,
    .priv_data_size = sizeof(mcenc_context_t),
    .init = mcenc_init,
    .encode2 = mcenc_encode2,
    .pix_fmts = (const enum AVPixelFormat[]){
        AV_PIX_FMT_RGB24
    }
};
