#ifndef AVCODEC_MINECRAFT_H
#define AVCODEC_MINECRAFT_H
#include "avcodec.h"
#include "internal.h"

#define MC_MAX_BLOCKS 4096

typedef enum mc_block_type_t {
    MC_BLOCK_AIR,
    MC_BLOCK_WOOL,
    MC_BLOCK_COMMAND = 17,
    MC_BLOCK_REP2,
    MC_BLOCK_MAX
} mc_block_type_t;

typedef struct mc_block_t {
    mc_block_type_t type;
    char command[128];
} mc_block_t;

typedef struct mc_layer_t {
    int width;
    int height;
    mc_block_t blocks[MC_MAX_BLOCKS];
} mc_layer_t;

#endif
