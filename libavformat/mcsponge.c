#include "avformat.h"
#include "internal.h"
#include "libavcodec/minecraft.h"
#include "libavcodec/avcodec.h"
#include "libavcodec/internal.h"
#include <zlib.h>

#define AV_TRY(cmd) do { \
    int ret = (cmd); \
    if (ret < 0) { \
        return ret; \
    } \
} while (0)

#define MC_MAX_LAYERS 1024
#define BUFFER_SIZE 65536
#define WINDOW_BITS 31
#define MEMORY_LEVEL 8

static const char* mcsponge_names[] = {
    "minecraft:air",
    "minecraft:white_wool",
    "minecraft:orange_wool",
    "minecraft:magenta_wool",
    "minecraft:light_blue_wool",
    "minecraft:yellow_wool",
    "minecraft:lime_wool",
    "minecraft:pink_wool",
    "minecraft:gray_wool",
    "minecraft:light_gray_wool",
    "minecraft:cyan_wool",
    "minecraft:purple_wool",
    "minecraft:blue_wool",
    "minecraft:brown_wool",
    "minecraft:green_wool",
    "minecraft:red_wool",
    "minecraft:black_wool",
    "minecraft:command_block",
    "minecraft:repeater[delay=2]"
};

typedef enum nbt_tag_t {
    NBT_TAG_END,
    NBT_TAG_BYTE,
    NBT_TAG_SHORT,
    NBT_TAG_INT,
    NBT_TAG_LONG,
    NBT_TAG_FLOAT,
    NBT_TAG_DOUBLE,
    NBT_TAG_BYTE_ARRAY,
    NBT_TAG_STRING,
    NBT_TAG_LIST,
    NBT_TAG_COMPOUND,
    NBT_TAG_INT_ARRAY,
    NBT_TAG_LONG_ARRAY
} nbt_tag_t;

typedef struct mcsponge_context_t {
    z_stream zlib;
    uint8_t buffer[BUFFER_SIZE];
    int index;
    int width;
    int height;
    int length;
    mc_layer_t layers[MC_MAX_LAYERS];
} mcsponge_context_t;

static int nbt_raw_flush(AVFormatContext* avctx, int finish) {
    int ret;
    mcsponge_context_t* ctx = avctx->priv_data;
    uint8_t buffer[BUFFER_SIZE];
    ctx->zlib.next_in = ctx->buffer;
    ctx->zlib.avail_in = ctx->index;

    for (;;) {
        ctx->zlib.next_out = buffer;
        ctx->zlib.avail_out = BUFFER_SIZE;
        if ((ret = deflate(&ctx->zlib, finish ? Z_FINISH : Z_NO_FLUSH)) != Z_OK) {
            if (finish && ret == Z_STREAM_END) {
                avio_write(avctx->pb, buffer, BUFFER_SIZE - ctx->zlib.avail_out);
                return 0;
            }
            av_log(avctx, AV_LOG_ERROR, "%s\n", ctx->zlib.msg);
            return AVERROR_EXTERNAL;
        }
        avio_write(avctx->pb, buffer, BUFFER_SIZE - ctx->zlib.avail_out);
        if (!finish && ctx->zlib.avail_in == 0) {
            ctx->index = 0;
            return 0;
        }
    }
}

static void nbt_raw_byte(AVFormatContext* avctx, int8_t value) {
    mcsponge_context_t* ctx = avctx->priv_data;
    ctx->buffer[ctx->index++] = value;
}

static void nbt_raw_short(AVFormatContext* avctx, int16_t value) {
    nbt_raw_byte(avctx, value >> 8);
    nbt_raw_byte(avctx, value & 0xFF);
}

static void nbt_raw_int(AVFormatContext* avctx, int32_t value) {
    nbt_raw_short(avctx, value >> 16);
    nbt_raw_short(avctx, value & 0xFFFF);
}

static void nbt_raw_long(AVFormatContext* avctx, int64_t value) {
    nbt_raw_int(avctx, value >> 32);
    nbt_raw_int(avctx, value & 0xFFFFFFFF);
}

static void nbt_raw_string(AVFormatContext* avctx, const char* value) {
    mcsponge_context_t* ctx = avctx->priv_data;
    int16_t size = strlen(value);
    nbt_raw_short(avctx, size);
    memcpy(ctx->buffer + ctx->index, value, size);
    ctx->index += size;
}

static int nbt_end(AVFormatContext* avctx) {
    nbt_raw_byte(avctx, NBT_TAG_END);
    return nbt_raw_flush(avctx, 0);
}

static int nbt_tag(AVFormatContext* avctx, nbt_tag_t tag, const char* name) {
    nbt_raw_byte(avctx, tag);
    nbt_raw_string(avctx, name);
    return nbt_raw_flush(avctx, 0);
}

static int nbt_short(AVFormatContext* avctx, const char* name, int16_t value) {
    AV_TRY(nbt_tag(avctx, NBT_TAG_SHORT, name));
    nbt_raw_short(avctx, value);
    return nbt_raw_flush(avctx, 0);
}

static int nbt_int(AVFormatContext* avctx, const char* name, int32_t value) {
    AV_TRY(nbt_tag(avctx, NBT_TAG_INT, name));
    nbt_raw_int(avctx, value);
    return nbt_raw_flush(avctx, 0);
}

static int nbt_string(AVFormatContext* avctx, const char* name, const char* value) {
    AV_TRY(nbt_tag(avctx, NBT_TAG_STRING, name));
    nbt_raw_string(avctx, value);
    return nbt_raw_flush(avctx, 0);
}

static mc_block_t* mcsponge_block(mcsponge_context_t* ctx, int x, int y, int z) {
    return ctx->layers[z].blocks + y * ctx->width + x;
}

static int mcsponge_query_codec(enum AVCodecID codec, int std) {
    return codec == AV_CODEC_ID_MINECRAFT;
}

static av_cold int mcsponge_init(AVFormatContext* avctx) {
    int ret;
    mcsponge_context_t* ctx = avctx->priv_data;
    if (
        avctx->nb_streams != 1 ||
        avctx->streams[0]->codecpar->codec_id != AV_CODEC_ID_MINECRAFT
    ) {
        av_log(avctx, AV_LOG_ERROR, "Only a single Minecraft stream is supported\n");
        return AVERROR(EINVAL);
    }

    memset(&ctx->zlib, 0, sizeof(z_stream));
    if ((ret = deflateInit2(
        &ctx->zlib,
        Z_DEFAULT_COMPRESSION,
        Z_DEFLATED,
        WINDOW_BITS,
        MEMORY_LEVEL,
        Z_DEFAULT_STRATEGY
    )) != Z_OK) {
        av_log(avctx, AV_LOG_ERROR, "%s\n", ctx->zlib.msg);
        return AVERROR_EXTERNAL;
    }
    return 0;
}

static int mcsponge_write_header(AVFormatContext* avctx) {
    AV_TRY(nbt_tag(avctx, NBT_TAG_COMPOUND, "Schematic"));
    AV_TRY(nbt_int(avctx, "Version", 2));
    AV_TRY(nbt_int(avctx, "DataVersion", 1343));

    AV_TRY(nbt_int(avctx, "PaletteMax", MC_BLOCK_MAX));
    AV_TRY(nbt_tag(avctx, NBT_TAG_COMPOUND, "Palette"));
    for (int i = 0; i < MC_BLOCK_MAX; ++i) {
        AV_TRY(nbt_int(avctx, mcsponge_names[i], i));
    }
    AV_TRY(nbt_end(avctx));
    return 0;
}

static int mcsponge_write_packet(AVFormatContext* avctx, AVPacket* pkt) {
    mcsponge_context_t* ctx = avctx->priv_data;
    mc_layer_t* layer = (mc_layer_t*)pkt->data;
    if (layer->width > ctx->width) {
        ctx->width = layer->width;
    }
    if (layer->height > ctx->height) {
        ctx->height = layer->height;
    }
    if (ctx->length == MC_MAX_LAYERS) {
        av_log(avctx, AV_LOG_ERROR, "Too many layers\n");
        return AVERROR(EINVAL);
    }
    ctx->layers[ctx->length++] = *layer;
    return 0;
}

static int mcsponge_write_trailer(AVFormatContext* avctx) {
    int ret;
    mcsponge_context_t* ctx = avctx->priv_data;
    int entities = 0;

    AV_TRY(nbt_short(avctx, "Width", ctx->width));
    AV_TRY(nbt_short(avctx, "Height", ctx->height));
    AV_TRY(nbt_short(avctx, "Length", ctx->length));

    AV_TRY(nbt_tag(avctx, NBT_TAG_BYTE_ARRAY, "BlockData"));
    nbt_raw_int(avctx, ctx->width * ctx->height * ctx->length);
    for (int y = 0; y < ctx->height; ++y) {
        for (int z = 0; z < ctx->length; ++z) {
            for (int x = 0; x < ctx->width; ++x) {
                mc_block_t* block = mcsponge_block(ctx, x, y, z);
                nbt_raw_byte(avctx, block->type);
                if (block->type == MC_BLOCK_COMMAND) {
                    ++entities;
                }
            }
            AV_TRY(nbt_raw_flush(avctx, 0));
        }
    }

    AV_TRY(nbt_tag(avctx, NBT_TAG_LIST, "BlockEntities"));
    nbt_raw_byte(avctx, NBT_TAG_COMPOUND);
    nbt_raw_int(avctx, entities);
    for (int y = 0; y < ctx->height; ++y) {
        for (int z = 0; z < ctx->length; ++z) {
            for (int x = 0; x < ctx->width; ++x) {
                mc_block_t* block = mcsponge_block(ctx, x, y, z);
                if (block->type != MC_BLOCK_COMMAND) {
                    continue;
                }

                AV_TRY(nbt_tag(avctx, NBT_TAG_INT_ARRAY, "Pos"));
                nbt_raw_int(avctx, 3);
                nbt_raw_int(avctx, x);
                nbt_raw_int(avctx, y);
                nbt_raw_int(avctx, z);
                AV_TRY(nbt_string(avctx, "Id", "minecraft:command_block"));
                AV_TRY(nbt_string(avctx, "Command", block->command));
                AV_TRY(nbt_end(avctx));
            }
        }
    }

    AV_TRY(nbt_end(avctx));
    nbt_raw_flush(avctx, 1);
    if ((ret = deflateEnd(&ctx->zlib)) != Z_OK) {
        av_log(avctx, AV_LOG_ERROR, "%s\n", ctx->zlib.msg);
        return AVERROR_EXTERNAL;
    }
    return 0;
}

AVOutputFormat ff_mcsponge_muxer = {
    .name = "mcsponge",
    .long_name = NULL_IF_CONFIG_SMALL("Minecraft Sponge Schematic"),
    .extensions = "schem",
    .priv_data_size = sizeof(mcsponge_context_t),
    .video_codec = AV_CODEC_ID_MINECRAFT,
    .audio_codec = AV_CODEC_ID_NONE,
    .subtitle_codec = AV_CODEC_ID_NONE,
    .query_codec = mcsponge_query_codec,
    .init = mcsponge_init,
    .write_header = mcsponge_write_header,
    .write_packet = mcsponge_write_packet,
    .write_trailer = mcsponge_write_trailer
};
